import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {

		ArrayList<Player> players = new ArrayList<Player>();
		ArrayList<Player> foldPlayers;
		Rulerable ruler;
		for (int i = 0 ; i < 5 ; i++ ) {
			System.out.println("플레이어 인원수");
			ruler = new Ruler();
			foldPlayers = ruler.startGame(players);
			System.out.println("GameEnd");
			
			restorePlayers(foldPlayers, players);
			
		}	
	}
	
	public static void restorePlayers(ArrayList<Player> foldPlayers, ArrayList<Player> players) {
		players.addAll(foldPlayers);
	}
	
	
}