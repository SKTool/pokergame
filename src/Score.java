
public class Score implements Scoreable {
	private int score;
	private int cardDummyCount;
	private int[][] pairCheck = new int[1][3];
	private int[][] myDummy = new int[7][2];
	Card[] tmpCardDummy = new Card[7];
	Card[] ckFourCard = new Card[7];
	
	@Override
	public void setScore(Cardable[] cardDummy, int cardDummyCount, boolean isLastCard) {
		
		for (int i = 0 ; i < 7 ; i++ ) {
			tmpCardDummy[i] = new Card();
		}
		
		for (int i = 0 ; i < 7 ; i++ ) {
			if (cardDummy[i] != null) {
				tmpCardDummy[i].makeCard(cardDummy[i].getCardNumber(), cardDummy[i].getCardPatten());
				
			}
		}
//		if(tmpCardDummy[2].getCardNumber() != null) {
		if(isLastCard) {
			countCardDummy(cardDummyCount);
			cvtToInt(tmpCardDummy);
		}else {
			nextTop();
			countCardDummy(cardDummyCount);
			cvtToInt(ckFourCard);
		}
		rangeArr();
		checkPair();
//			if(ckFourCard[1].getCardNumber() != null) {
//			}
//		}
	}
	
	public int getScore() {
		if(royalStraightFlush(myDummy) != 0) {
			return royalStraightFlush(myDummy);
		}else if(backStraightFlush(myDummy) != 0) {
			return backStraightFlush(myDummy);
		}else if(straightFlush(myDummy) != 0) {
			return straightFlush(myDummy);
		}else if(fourCard(pairCheck) != 0) {
			return fourCard(pairCheck);
		}else if(fullhouse(pairCheck) != 0) {
			return fullhouse(pairCheck);
		}else if(flush(myDummy) != 0) {
			return flush(myDummy);
		}else if(mountain(myDummy) != 0) {
			return mountain(myDummy);
		}else if(backStraight(myDummy) != 0) {
			return backStraight(myDummy);
		}else if(straight(myDummy) != 0) {
			return straight(myDummy);
		}else if(triple(pairCheck) != 0) {
			return triple(pairCheck);
		}else if(twoPair( pairCheck) != 0) {
			return twoPair(pairCheck);
		}else if(onePair(pairCheck) != 0) {
			return onePair(pairCheck);
		}
		return  top(myDummy);
	}
		
	public void nextTop() {
		int count = 0;
		for (int i = 0 ; i < 7 ; i++ ) {
			ckFourCard[i] = new Card();
		}
		for (int i = 0; i < tmpCardDummy.length; i++) {
			if(i == 2 || i == 3 || i == 4 || i == 5) {
				ckFourCard[count] = tmpCardDummy[i];
			} 
			count++;
		}
	}
	
	private void countCardDummy(int cardDummyCount) {
		this.cardDummyCount = cardDummyCount;
	}

	public void cvtToInt(Cardable[] playerCardDummy) {

		// 카드숫자가 들어가는 배열
		String[] st = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };
		String[] st2 = { "♣", "♥", "◆", "♠" };
		// 카드 숫자를 하나씩 넣어서 myDummy에 int값을 부여하는 for문
		
		for (int i = 0; i < this.cardDummyCount; i++) {
			if(playerCardDummy[i].getCardNumber() == null) {
				continue;
			}
			for (int j = 0; j < st.length; j++) {
				// 1은 가장 크기때문에 따로 14 할당
				
				if (playerCardDummy[i].getCardNumber().equals("1")) {
					myDummy[i][0] = 14;
				}
				// st배열에있는 1을제외한 카드숫자전체를 집어넣어서 숫자로 변환
				if (playerCardDummy[i].getCardNumber().equals(st[j])) {
					myDummy[i][0] = j + 2;
				}
			}
		}
		for (int i = 0; i < this.cardDummyCount; i++) {
			if(playerCardDummy[i].getCardNumber() == null) {
				continue;
			}
			for (int j = 0; j < st2.length; j++) {
				if (playerCardDummy[i].getCardPatten().equals(st2[j])) {
					myDummy[i][1] = j + 1;
				}
			}
		}
	}

	public void rangeArr() {
		int[][] value = new int[1][2]; // 버블정렬할때 값 잠깐 담아놓을 변수
		for (int i = 0; i < this.cardDummyCount - 1; i++) { // 버블정렬

			for (int j = 0; j < this.cardDummyCount - 1 - i; j++) {

				if (myDummy[j][0] == myDummy[j + 1][0]) {
					if (myDummy[j][1] > myDummy[j + 1][1]) {
						value[0][1] = myDummy[j + 1][1];
						myDummy[j + 1][1] = myDummy[j][1];
						myDummy[j][1] = value[0][1];
					}
				}

				if (myDummy[j][0] > myDummy[j + 1][0]) {
					value[0][0] = myDummy[j + 1][0];
					value[0][1] = myDummy[j + 1][1];
					myDummy[j + 1][0] = myDummy[j][0];
					myDummy[j + 1][1] = myDummy[j][1];
					myDummy[j][0] = value[0][0];
					myDummy[j][1] = value[0][1];
				}
			}
		}
	}
	
	public void checkPair() {
		// #1-1 해결 pairCheck 을 초기화
		pairCheck = new int[1][3];	
		int[][] forPairCheck = null;
		int count = 0;
		int stop = 0;
		for (int i = 0; i < this.cardDummyCount - 1; i++) {
			if ( myDummy[i][0] != 0 &&  myDummy[i][0] == myDummy[i + 1][0]) {
				for (int j = 0; j < pairCheck.length; j++) {
					// #1 문제 ..... 전에 있던 pairCheck 배열에 다시 더한다. #1-1 해결 
					if (myDummy[i+1][0] == pairCheck[j][0]) {
						pairCheck[j][1]++;
						pairCheck[j][2] = myDummy[i+1][1];
						break;
					}
					stop++;
				}
				if (stop == pairCheck.length) {
					count++;
					forPairCheck = new int[count][3];
					for (int k = 0; k < pairCheck.length; k++) {
						for (int v = 0; v < 3; v++) {
							forPairCheck[k][v] = pairCheck[k][v];
						}
					}

					forPairCheck[count - 1][0] = myDummy[i+1][0];
					forPairCheck[count - 1][1]++;
					forPairCheck[count - 1][2] = myDummy[i+1][1];
					pairCheck = forPairCheck;
					stop = 0;
				}
			}
		}
		
		
//		for(int i = 0; i < pairCheck.length; i++) {
//			for(int j = 0; j < pairCheck[0].length; j++) {
//				System.out.print(pairCheck[i][j] + " ");
//			}
//			System.out.print("\n");
//		}


	}

//	public void toScore() {
//		for(int i = 0; i < pairCheck.length; i++) {
//			if(pairCheck[i][2] > 0) {
//				
//			}
//		}
//	}
	
///////////////////////////////////////////////////////////////////////////////////////
	public int top(int[][] myDummy) {				// 21~144
		return (myDummy[cardDummyCount-1][0] * 10) + (myDummy[cardDummyCount-1][1]);
	}
	
	public int onePair(int[][] pairCheck) {			// 147~1026
		if(pairCheck.length > 0) {
			return (pairCheck[pairCheck.length-1][0] * 73) + (pairCheck[pairCheck.length-1][1]);
		}
		return 0;
	}
	
	public int twoPair(int[][] pairCheck) {			// 1027~7186
		if(pairCheck.length > 1) {
			return (pairCheck[pairCheck.length-1][0] * 513) + (pairCheck[pairCheck.length-1][1]);
		}
		return 0;
	}
	
	public int triple(int[][] pairCheck) {			// 7187~50306

		for(int i = 0; i < pairCheck.length; i++) {
			if(pairCheck[i][1] == 2) {
				return (pairCheck[pairCheck.length-1][0] * 3593) + (pairCheck[pairCheck.length-1][1]);
			}
		}
		return 0;
	}
	
	public int straight(int[][] myDummy) {			// 50311-98609
		int t = 8385;
		for(int l = 0; l < 8; l++) {
			for(int i = 0; i < myDummy.length; i++) {
				if(myDummy[i][0] == 2+l) {
					
					for(int j = 0; j < myDummy.length; j++) {
						if(myDummy[j][0] == 3+l) {
	
							for(int k = 0; k < myDummy.length; k++) {
								if(myDummy[k][0] == 4+l) {
	
									for(int v = 0; v < myDummy.length; v++) {
										if(myDummy[v][0] == 5+l) {
	
											for(int s = 0; s < myDummy.length; s++) {
												if(myDummy[s][0] == 6+l) {
													return (myDummy[s][0] * t) + (myDummy[s][1]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			t -= 100;
		}
		return 0;
	}
	
	public int backStraight(int[][] myDummy) {		// 98617-98620
		for(int i = 0; i < myDummy.length; i++) {
			if(myDummy[i][0] == 14) {
				
				for(int j = 0; j < myDummy.length; j++) {
					if(myDummy[j][0] == 2) {

						for(int k = 0; k < myDummy.length; k++) {
							if(myDummy[k][0] == 3) {

								for(int v = 0; v < myDummy.length; v++) {
									if(myDummy[v][0] == 4) {

										for(int s = 0; s < myDummy.length; s++) {
											if(myDummy[s][0] == 5) {
												return (myDummy[i][0] * 7044) + (myDummy[i][1]) + 1300;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return 0;
	}
	
	public int mountain(int[][] myDummy) {			// 98631-98634
		for(int i = 0; i < myDummy.length; i++) {
			if(myDummy[i][0] == 10) {
				
				for(int j = 0; j < myDummy.length; j++) {
					if(myDummy[j][0] == 11) {

						for(int k = 0; k < myDummy.length; k++) {
							if(myDummy[k][0] == 12) {

								for(int v = 0; v < myDummy.length; v++) {
									if(myDummy[v][0] == 13) {

										for(int s = 0; s < myDummy.length; s++) {
											if(myDummy[s][0] == 14) {
												return (myDummy[s][0] * 7045) + (myDummy[s][1]) + 1300;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return 0;
	}
	
	public int flush(int[][] myDummy){ 				// 100002-400014
		
		for(int i = 4; i > 0; i--) {
			
			int[][] tmp = new int[1][1];
			int count = 0;
			
			for(int j = 0; j < myDummy.length; j++) {
				if(myDummy[j][1] == i) {
					tmp[0][0] = myDummy[j][0];
					count++;
				}
			}
			
			if(count == 5) {
				return  (i * 100000) + (tmp[0][0]);
			}
		}
		return 0;
	}
	
	public int fullhouse(int[][] pairCheck) {		// 400015-2800102
		for(int i = 0; i < pairCheck.length; i++) {
			if(pairCheck[i][1] == 2) {
				for(int j = 0; j < pairCheck.length; j++) {
					if(pairCheck[j][1] == 1) {
						return (pairCheck[i][0] * 200007) + (pairCheck[i][2]);
					}
				}
			}
		}
		return 0;
	}
	
	public int fourCard(int[][] pairCheck) { 		// 1260202-19600714
		for(int i = 0; i < pairCheck.length; i++) {
			if(pairCheck[i][1] == 3) {
				return (pairCheck[i][0] * 1400051) + 1;
			}
		}
		return 0;
	}
	//fourCard ckFourCard로 바꿈
	public int straightFlush(int[][] myDummy) {		// 19600711-42468222
		for(int l = 0; l < 8; l++) {
			for(int i = 0; i < myDummy.length; i++) {
				if(myDummy[i][0] == 2+l) {
					
					for(int j = 0; j < myDummy.length; j++) {
						if(myDummy[j][0] == 3+l) {
	
							for(int k = 0; k < myDummy.length; k++) {
								if(myDummy[k][0] == 4+l) {
	
									for(int v = 0; v < myDummy.length; v++) {
										if(myDummy[v][0] == 5+l) {
	
											for(int s = 0; s < myDummy.length; s++) {
												if(myDummy[s][0] == 6+l) {
													for(int p = 4; p > 0; p--) {
														int count = 0;
														
														for(int y = 0; y < myDummy.length; y++) {
															if(myDummy[y][1] == p) {
																if(y == i || y == j || y == k || y == v || y == s) {	
																count++;
																}
															}
														}
														if(count == 5) {
															return  ((myDummy[s][0] * 3266786) + (myDummy[s][1]));
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return 0;
	}
	
	public int backStraightFlush(int[][] myDummy) { // 42468231-42468234
		for(int i = 0; i < myDummy.length; i++) {
			if(myDummy[i][0] == 14) {
				
				for(int j = 0; j < myDummy.length; j++) {
					if(myDummy[j][0] == 2) {

						for(int k = 0; k < myDummy.length; k++) {
							if(myDummy[k][0] == 3) {

								for(int v = 0; v < myDummy.length; v++) {
									if(myDummy[v][0] == 4) {

										for(int s = 0; s < myDummy.length; s++) {
											if(myDummy[s][0] == 5) {
												
												for(int n = 4; n > 0; n--) {
													int count = 0;
													for(int m = 0; m < myDummy.length; m++) {
														if(myDummy[m][1] == n) {
															count++;
														}
													}
													if(count == 5) {
														return (myDummy[i][0] * 3033445) + (myDummy[i][1]);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return 0;
	}
	
	public int royalStraightFlush(int[][] myDummy) {// 42468245-42468248
		for(int i = 0; i < myDummy.length; i++) {
			if(myDummy[i][0] == 10) {
				
				for(int j = 0; j < myDummy.length; j++) {
					if(myDummy[j][0] == 11) {

						for(int k = 0; k < myDummy.length; k++) {
							if(myDummy[k][0] == 12) {

								for(int v = 0; v < myDummy.length; v++) {
									if(myDummy[v][0] == 13) {

										for(int s = 0; s < myDummy.length; s++) {
											if(myDummy[s][0] == 14) {
												
												for(int n = 4; n > 0; n--) {
													int count = 0;
													for(int m = 0; m < myDummy.length; m++) {
														if(myDummy[m][1] == n) {
															if(m == i || m == j || m == k || m == v || m == s) {
															count++;
															}
														}
													}
													if(count == 5) {
														return (myDummy[s][0] * 3033446) + (myDummy[s][1]);
													}
												}
												
												
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return 0;
	}	
}