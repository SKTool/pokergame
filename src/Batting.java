import java.util.Scanner;

public class Batting implements Battingable {

	private int payedMoneyCurrentTurn;
	
	// TODO 수정 필요
	private static int battingTurnCount;
	
	private int currentGameBoardMoney;
	private int previousPlayerMoney;
	
	@Override
	public int calcBatting(Player player, int currentGameBoardMoney, int previousPlayerMoney) {
		Scanner choseBatting = new Scanner(System.in);
		boolean isBatting = false;
		int allMoney = player.giveAllMoney();
		while (true) {
			System.out.print("베팅을 하라 : ");
			int battingOption = choseBatting.nextInt();
			isBatting = checkBatting(battingOption, battingTurnCount, allMoney);
			if (isBatting) {
				switch (battingOption) {
				case 0:
					// Fold
					player.setPlayerState("Fold");
					return 0;
				case 1:
					player.setPlayerState("Check");
					return 0;
				case 2:
					player.setPlayerState("Call");
					return previousPlayerMoney - payedMoneyCurrentTurn;
				case 3:
					player.setPlayerState("Half");
					return currentGameBoardMoney / 2;
				case 4:
					player.setPlayerState("AllIn");
					battingAllIn(previousPlayerMoney);
					return allMoney;
				default:
					System.out.println("존재하지 아는 배팅 옵션입니다.");
					continue;
				}
			} else {
				System.out.println("현재 플레이어의 상태로는 진행할 수 없는 배팅옵션입니다.");
			}
		}
	}

	private boolean checkBatting(int battingOption, int battingTurnCount, int allMoney) {
		if (battingOption == 4 && previousPlayerMoney < allMoney ) {
			return false;
		} else if (previousPlayerMoney > allMoney) {
			return false;
		} else if (battingOption == 3 && currentGameBoardMoney / 2 > allMoney) {
			return false;
		} else if (battingOption == 2 && battingTurnCount == 1) {
			return false;
		} else if (battingOption == 1 && battingTurnCount != 1) {
			return false;
		}  else {
			return true;
		}
	}
	
	private void battingAllIn(int previousPlayerMoney) {
		addPayedMoneyCurrentTurn(previousPlayerMoney);
	}
	
	@Override
	public void viewBatting() {
		System.out.println("0 - Hold");
		System.out.println("1 - Check");
		System.out.println("2 - Call");
		System.out.println("3 - Half");
		System.out.println("4 - AllIn");
	}
	
	@Override
	public void addPayedMoneyCurrentTurn(int payedMoneyCurrentTurn) {
		this.payedMoneyCurrentTurn += payedMoneyCurrentTurn;
	}
	
	@Override
	public void setPayedMoneyCurrentTurn(int payedMoneyCurrentTurn) {
		this.payedMoneyCurrentTurn = payedMoneyCurrentTurn;
	}
	
	@Override 
	public void addBattingTurnCount(int battingTurnCount) {
		this.battingTurnCount += battingTurnCount;
	}
	
	@Override	
	public void setBattingTurnCount(int battingTurnCount) {
		this.battingTurnCount = battingTurnCount;
	}
	
	@Override
	public int getPayedMoneyCurrentTurn() {
		return payedMoneyCurrentTurn;
	}
}
