import java.util.ArrayList;

public interface Rulerable {
	public ArrayList<Player> startGame(ArrayList<Player> players);
}
