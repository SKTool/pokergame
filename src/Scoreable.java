
public interface Scoreable {
	void setScore(Cardable[] cardDummy, int cardDummyCount, boolean isLastCard);

	int getScore();

}
