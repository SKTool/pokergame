
public class Wallet implements Walletable {

	private final static int INIT_MONEY = 500000;
	private int money = 0;
	
	Wallet() {
		money = INIT_MONEY;
	}
	
	@Override
	public void showMoney() {
		// TODO Auto-generated method stub
		System.out.println(money);
	}

	@Override
	public boolean spendMoney(int payment) {
		// TODO Auto-generated method stub
		
		if ( money >= payment) {
			money -= payment;
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int getAllMoney() {
		// TODO Auto-generated method stub
		
		return money;
	}
	
	@Override 
	public void takeInWallet(int money) {
		this.money += money;
	}
}
