
public interface Walletable {

	// 돈을 사용(소비)하는 메서드
	public boolean spendMoney(int payment);
	
	// 전재산을 가져오는 메서드
	public int getAllMoney();
	
	// 돈을 보여는 메서드
	public void showMoney();
	
	public void takeInWallet(int money);
}
