
public interface CardDeckable {

	// 카드를 가져오는 메서드
	public Cardable takeCard();
	
	// 싱글톤 을 위해서 사용하는 메서드
	//public static CardDeckable getInstance()
}
