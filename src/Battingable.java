
public interface Battingable {
	public int calcBatting(Player player, int currentGameBoardMoney, int previousPlayerMoney);
	
	public void viewBatting();

	public void addPayedMoneyCurrentTurn(int payedMoneyCurrentTurn);
	public void setPayedMoneyCurrentTurn(int payedMoneyCurrentTurn);
	public int getPayedMoneyCurrentTurn();
	
	public void addBattingTurnCount(int battingTurnCount);
	public void setBattingTurnCount(int battingTurnCount);
}
