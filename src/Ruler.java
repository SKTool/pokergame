import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Ruler implements Rulerable {
	
	private final static int NEXT_AREA = 1;	
	private final static int INIT_ENTRY_FEE = 1000;
	private final static int INIT_CARD_DUMMY = 5;

	private static CardDeckable cardDeck;
	private int currentGameBoardMoney = 0;
	
	Ruler() {
		cardDeck = CardDeck.getInstance();
	}
	
	@Override
	public ArrayList<Player> startGame(ArrayList<Player> players) {
		
		if ( !makePlayers(players) ) 
			return null;
		
		ArrayList<Player> foldPlayers = new ArrayList<Player>();
		
		int[] battingOrderTokens = new int[3];
		battingOrderTokens[0] = 0;
		battingOrderTokens[1] = players.size() - 1;
		battingOrderTokens[2] = battingOrderTokens[0] + 1;

		
		boolean isDropTerms = true;
		boolean isGameEnd = false;
		
		while (true) {
//			for ( int i = 0 ; !isGameEnd && isDropTerms && i < players.size() ; i++ ) {
			for ( int i = 0 ; isDropTerms && i < players.size() ; i++ ) {
				players.get(i).dropCard(cardDeck);
				
				if (players.get(i).getPlayerState().equals("DropEnd")) {
					isGameEnd = true;
				}
			}
			
			if ( players.get(battingOrderTokens[0]).getCardDummyCount() == 3) {
				choseThreeCard(players);
			}
			
			sortBossIndex(players);
			
			viewPlayerInfo(players);
			
			if ( players.get(battingOrderTokens[0]).getCardDummyCount() > INIT_CARD_DUMMY) {
				
				if ( isGameEnd ) {
					sendMoneyToWinner(players);
					break;
				}
				
				viewPlayerCheckMessage();
				players.get(battingOrderTokens[0]).showPlayerCardDummy();
				currentGameBoardMoney += players.get(battingOrderTokens[0]).batting(
																currentGameBoardMoney, 
																players.get(battingOrderTokens[1]).givePayedMoneyCurrentTurn()
													);
			
				clearScreen();
				checkFoldBatting(foldPlayers, players, battingOrderTokens);
				
				isDropTerms = endBatting(players, battingOrderTokens);
				
				nextBattingOrderTokens(battingOrderTokens, players.size());	
			}
		}

		currentGameBoardMoney = 0;
		initPlayerCardDummy(players);
		initPlayerCardDummy(foldPlayers);
		
		return foldPlayers;
	}
	
	private void initPlayerCardDummy(ArrayList<Player> players) {
		for ( int i = 0 ; i < players.size() ; i++ ) {
			players.get(i).initCardDummy();
		}
	}
	
	private void sendMoneyToWinner(ArrayList<Player> players) {
		decideWinner(players);
		
		sortBossIndex(players);
		
		players.get(0).setMoney(currentGameBoardMoney);
		
	}
	
	private void decideWinner(ArrayList<Player> players) {
		for (int i = 0 ; i < players.size(); i ++ ) {
			players.get(i).giveLastScore();
			System.out.println("["+i+1+"] 플레이어");			
			players.get(i).showPlayerScore();;
			players.get(i).showPlayerCardDummy();
			System.out.println();
		}
	}
	
	private boolean makePlayers(ArrayList<Player> players) {
		Scanner inputPeople= new Scanner(System.in);
		int people = inputPeople.nextInt();
		
		if (people > 6) {
			System.out.println("게임방이 가득 찼습니다..");
			return false;
		}
		
		for (int i = 0 ; i < people ; i++ ) {
			if ( i >= players.size() ) {
				players.add(new Player());	
			}
			entryFee(players.get(i));
		}
		return true;
	}
	
	private boolean endBatting(ArrayList<Player> players, int[] battingOrderTokens) {	
		
		if ( players.size() == 1 ) {
			return false;
		}
		
		boolean isZeroPayedMoneyTurn = players.get(battingOrderTokens[0]).givePayedMoneyCurrentTurn() != 0;
		boolean equalsPreviousPlayerPayedMomey = players.get(battingOrderTokens[0]).givePayedMoneyCurrentTurn() == players.get(battingOrderTokens[1]).givePayedMoneyCurrentTurn();
		boolean equalsNextPlayerPayedMomey = players.get(battingOrderTokens[0]).givePayedMoneyCurrentTurn() == players.get(battingOrderTokens[2]).givePayedMoneyCurrentTurn();
		
		if ( isZeroPayedMoneyTurn && equalsPreviousPlayerPayedMomey && equalsNextPlayerPayedMomey ) {
			return true;
		} else {
			return false;
		}
		
	}
	
	// 정렬의 원리 이해 , Player화 Playerable
	private void sortBossIndex(ArrayList<Player> players) {
		Player player = Collections.max(players);
		int firstIndex = players.indexOf(player);
		
		for ( int i = 0 ; i < firstIndex ; i++ ) {
			players.add(players.get(0));
			players.remove(players.get(0));
		}
	}
	
	private void viewPlayerCheckMessage() {
		Scanner inputCardIndex = new Scanner(System.in);
		
		do{
			System.out.print("플레이어가 맞습니까? (y/n) : ");
		} while ( !inputCardIndex.next().equals("y") );
		
	}
	
	private void choseThreeCard(ArrayList<Player> players) {
		Scanner inputCardIndex = new Scanner(System.in);
		int choseIndex;
		
		for (int i = 0 ; i < players.size() ; i++) {
			
			viewPlayerCheckMessage();
			
			players.get(i).showPlayerCardDummy(0,3);
			System.out.print("오픈할 카드를 선택해주세요. : ");
			choseIndex = inputCardIndex.nextInt() - 1;
			players.get(i).openSeletedCard(choseIndex);
			
			clearScreen();
		}
	}
	
	private static void clearScreen() {
		for (int i = 0; i < 80; i++)
			System.out.println("");
	}
	
	
	private void nextBattingOrderTokens(int[] playersIndex, int numOfPlayers) {
		playersIndex[0] += NEXT_AREA;
		releaseBattingOrderTokens(playersIndex, numOfPlayers);
	}
	
	private void releaseBattingOrderTokens( int[] playersIndex, int numOfPlayers ) {
		playersIndex[0] = ( playersIndex[0] ) % numOfPlayers;	
		playersIndex[1] = ( playersIndex[0] - 1 ) % numOfPlayers < 0 ? 
				numOfPlayers - 1 : playersIndex[0] - 1;		
		playersIndex[2] = ( playersIndex[0] + 1 ) % numOfPlayers;	
	}
	
	private void entryFee(Player player) {
		this.currentGameBoardMoney += INIT_ENTRY_FEE;
		player.payMoney(INIT_ENTRY_FEE);
	}
	
	private void viewPlayerInfo( ArrayList<Player> players ) {
		for (int i = 0 ; i < players.size() ; i++ ) {
			System.out.println( " [ " + ( i + 1 ) + "플레이어 ] ");
			players.get(i).showPlayerCardDummy(2, 6);
			System.out.print("점수");
			players.get(i).showPlayerScore();
			
			System.out.print("지불한 금액 : ");
			System.out.print( players.get(i).givePayedMoneyCurrentTurn() );
			System.out.println(" 원 ");
			System.out.println();
			System.out.println();	
			System.out.print("현재 전재산 : ");
			players.get(i).showPlayerMoney();
			System.out.println();

		}
	}
	
	private void checkFoldBatting(ArrayList<Player> foldPlayers, ArrayList<Player> players, int[] battingOrderTokens) {
		if ( players.get(battingOrderTokens[0]).getPlayerState().equals("Fold")) {
			foldPlayers.add(players.get(battingOrderTokens[0]));			
			players.remove(battingOrderTokens[0]);			
		}		
		releaseBattingOrderTokens(battingOrderTokens, players.size());
	}
}

class AscendingPlayerable implements Comparator<Integer> {
	
	@Override 
	public int compare(Integer a, Integer b) { 
		return b.compareTo(a); 
		
	} 
}