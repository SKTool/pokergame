
public class CardDeck implements CardDeckable{

// 싱글톤 (디자인 패턴) 사용했다.
	private static CardDeckable instance = new CardDeck();

	public static CardDeckable getInstance() {
		return instance;
	}
	
	///////////////////////////////////////////////////////////
	
	private final static int NUM_OF_CARDDECK = 52;
	
	private Cardable[] cardDeck;
	
	private String[] cardNumber = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
	private String[] cardPatten = { "◆", "♥","♣", "♠" };
	
	private static int cardCount;
	
	CardDeck() {
		cardDeck = new Card[NUM_OF_CARDDECK];
		
		for (int i = 0 ; i < NUM_OF_CARDDECK ; i++ ) {
			cardDeck[i] = new Card();
		}
		
		prepareCard(cardDeck);
		
	}
	
	private void prepareCard(Cardable[] cardDeck) {
		int ranIndex;
		cardCount = NUM_OF_CARDDECK;
		int cardNumberCount = 0;
		int cardPattenCount = 0;
		
		while ( true ) {
			ranIndex = (int)(Math.random()*52);
			if ( cardDeck[ranIndex].getCardPatten() == null && 
					cardDeck[ranIndex].getCardNumber() == null ) {
				cardDeck[ranIndex].makeCard( cardNumber[cardNumberCount++], cardPatten[cardPattenCount] );
				
				if (cardNumberCount % 13 == 0) {
					cardNumberCount = 0;
					cardPattenCount++;
				}
				cardCount--;
			}
			if ( cardCount == 0 ) {
				break;
			}		
		}
	}
	


	@Override
	public Cardable takeCard() {
		// TODO Auto-generated method stub
		return this.cardDeck[cardCount++];
	}


	
}
