
public interface Playerable {
	public void setPlayerState(String playerState);
	
	public String getPlayerState();
	
	public boolean payMoney(int paymentMoney);
	
	// 게임을 사작하는 메서드 
	public void dropCard(CardDeckable cardDeck);
	// 베팅을 하는 메서드 
	public int batting( int currentGameBoardMoney, int previousPlayerMoney );
	
	// 플레이어 카드를 보여주는 메서드
	public void showPlayerCardDummy();
	public void showPlayerCardDummy(int firstCardIndex, int lastCardIndex);
	// 플레이어가 가지고 있는 메서드	
	public void showPlayerMoney();
	
	public void openSeletedCard(int selectionIndex);
	
	public int givePayedMoneyCurrentTurn();

	public int giveAllMoney();
	
	public int getCardDummyCount();
	
	public void giveLastScore();
	
	public void initCardDummy();
	
	public void setMoney(int money);
	
	public void showPlayerScore();
}
