
public interface Cardable {
	
	// 카드를 만드는 메서드
	public void makeCard(String number, String patten);
	// 카드에 모양을 가져오는 메서드
	public String getCardPatten();
	// 카드에 숫자를 가져오는 메서드
	public String getCardNumber();
	
}


