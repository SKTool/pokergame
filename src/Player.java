import java.util.Scanner;

public class Player implements Playerable, Comparable<Player>  {

	private final static int MAX_NUM_OF_CARD = 7;	

	private Cardable[] myCardDummy;
	private Walletable myWallet;
	private int cardDummyCount = 0;

	private String playerState = "Normal";

	private Scoreable myScore;
	private Batting myBatting;
	
	Player() {
		this.myBatting = new Batting();
		this.myScore = new Score();
		this.myCardDummy = new Card[MAX_NUM_OF_CARD];
		this.myWallet = new Wallet();
	}

	@Override
	public void initCardDummy() {
		this.myCardDummy = new Card[MAX_NUM_OF_CARD];
		this.myScore = new Score();
		
		this.playerState = "Normal";
		this.cardDummyCount = 0;
	}
	
	@Override
	public void giveLastScore() {
		this.myScore.setScore(this.myCardDummy, this.cardDummyCount, true);

	}
	
	@Override
	public void dropCard(CardDeckable cardDeck) {
		// TODO Auto-generated method stub
		if (this.cardDummyCount == MAX_NUM_OF_CARD) {
			playerState = "DropEnd";
		} else {
			System.out.println("카드를 뽑습니다.");
			new Scanner(System.in).nextLine();
			
			this.myCardDummy[this.cardDummyCount++] = cardDeck.takeCard();
			this.myScore.setScore(this.myCardDummy, this.cardDummyCount, false);

			myBatting.setPayedMoneyCurrentTurn(0);
			myBatting.setBattingTurnCount(1);
			

			playerState = "Normal";
		}
	}

	@Override
	public int batting(int currentGameBoardMoney, int previousPlayerMoney) {
		// TODO Auto-generated method stub

		myBatting.viewBatting();
		int payment = myBatting.calcBatting(this, currentGameBoardMoney, previousPlayerMoney);
		myBatting.addPayedMoneyCurrentTurn(payment);

		this.myWallet.spendMoney(payment);
		myBatting.addBattingTurnCount(1);
		return payment;

	}
	
	@Override
	public void showPlayerCardDummy() {
		showPlayerCardDummy(0,cardDummyCount);
	}
	
	@Override
	public void showPlayerCardDummy(int firstCardIndex, int lastCardIndex) {
		// TODO Auto-generated method stub	
		for (int i = firstCardIndex ; i < lastCardIndex && this.myCardDummy[i] != null  ; i++) {
			System.out.printf(" [ %s ] [ %s ] 현재 플레이어가 가지고 있는 카드  \n", this.myCardDummy[i].getCardNumber(),
					this.myCardDummy[i].getCardPatten());
		}
	}

	@Override
	public void showPlayerScore() {
		System.out.println(this.myScore.getScore());
	}
	
	@Override
	public void showPlayerMoney() {
		// TODO Auto-generated method stub
		this.myWallet.showMoney();
	}

	@Override
	public int compareTo(Player player) {
		// TODO Auto-generated method stub3
		return Integer.toString(myScore.getScore()).compareTo(Integer.toString(player.myScore.getScore()));
	}

	@Override
	public void openSeletedCard(int selectionIndex) {
		Card[] tempCardDummy = new Card[MAX_NUM_OF_CARD];
		for (int i = 0 ; i < 3 ; i++ ) {
			tempCardDummy[i] = new Card();
			tempCardDummy[i].makeCard(myCardDummy[i].getCardNumber(), myCardDummy[i].getCardPatten());

		}
		tempCardDummy[2].makeCard(myCardDummy[selectionIndex].getCardNumber(), myCardDummy[selectionIndex].getCardPatten());
		tempCardDummy[selectionIndex].makeCard(myCardDummy[2].getCardNumber(), myCardDummy[2].getCardPatten());
		myCardDummy = tempCardDummy;
	}
	
	@Override
	public void setPlayerState(String playerState) {
		this.playerState = playerState;
	}
	
	@Override
	public String getPlayerState() {
		return playerState;
	}

	@Override
	public int givePayedMoneyCurrentTurn() {
		return myBatting.getPayedMoneyCurrentTurn();
	}
	
	@Override
	public int getCardDummyCount() {
		return cardDummyCount;

	}
	
	@Override
	public int giveAllMoney() {
		return myWallet.getAllMoney();
	}
	
	@Override
	public void setMoney(int money) {
		this.myWallet.takeInWallet(money);
	}
	
	@Override
	public boolean payMoney(int paymentMoney) {
		// TODO Auto-generated method stub
		return this.myWallet.spendMoney(paymentMoney);
	}
	
}
